FROM node:16.13.2

WORKDIR /app

COPY package*.json ./

RUN npm install

COPY src /app/
COPY nest-cli.json  /app/
COPY tsconfig.build.json /app/
COPY tsconfig.json /app/
COPY .eslintrc.js /app/
COPY .prettierrc /app/

RUN npm run build

CMD ["npm", "run", "start:prod"]